from copy import deepcopy
import os
from cottonwood.activation import Logistic, ReLU, TanH
from cottonwood.experimental.online_normalization_2d \
    import OnlineNormalization2D
from cottonwood.conv2d import Conv2D
from cottonwood.linear import Linear
from cottonwood.loggers import ValueLogger
from cottonwood.loss import SquareLoss
from cottonwood.normalization import Bias
from cottonwood.operations import Copy, Flatten, HardMax, OneHot
from cottonwood.optimization import Adam
from cottonwood.pooling import MaxPool2D
from cottonwood.structure import load_structure, save_structure, Structure
from data import TrainingData

debug = False
if debug:
    n_iter_train = int(5e1)
    n_iter_report = int(1e2)
    n_iter_save = int(2e2)
else:
    n_iter_train = int(1e7)
    n_iter_report = int(1e3)
    n_iter_save = int(1e4)

kernel_size = 3
learning_rate = 1e-5
n_categories = 10
n_hidden_nodes = 128
n_kernels = 16

model_filename = "classifier.pkl"
reports_dir = "reports"


def main():
    try:
        model = retrieve_model()
    except Exception:
        model = create_model()

    train_model(model)


def retrieve_model():
    model = load_structure(model_filename)
    model.add(TrainingData(), "training_data")
    model.connect("training_data", "input_norm", i_port_tail=0)
    model.connect("training_data", "onehot", i_port_tail=1)
    return model


def create_model():
    optimizer = Adam(learning_rate=learning_rate)

    # First build two sizes of convolution blocks, each consisting of
    # convolution, normalization, a learned bias, and a nonlinearity.
    # The only difference between the two sizes is the number of kernels.
    small_conv = Structure()
    small_conv.add(Conv2D(
        kernel_size=kernel_size,
        n_kernels=n_kernels,
        optimizer=optimizer,
    ), "conv")
    small_conv.add(OnlineNormalization2D(), "norm")
    small_conv.add(Bias(optimizer=optimizer), "bias")
    small_conv.add(ReLU(), "activation")
    small_conv.connect_sequence(["conv", "norm", "bias", "activation"])

    large_conv = Structure()
    large_conv.add(Conv2D(
        kernel_size=kernel_size,
        n_kernels=4 * n_kernels,
        optimizer=optimizer,
    ), "conv")
    large_conv.add(OnlineNormalization2D(), "norm")
    large_conv.add(Bias(optimizer=optimizer), "bias")
    large_conv.add(ReLU(), "activation")
    large_conv.connect_sequence(["conv", "norm", "bias", "activation"])

    dense_layer = Structure()
    dense_layer.add(Linear(
        n_hidden_nodes,
        optimizer=optimizer,
    ), "linear")
    dense_layer.add(Bias(optimizer=optimizer), "bias")
    dense_layer.add(TanH(), "activation")
    dense_layer.connect_sequence(["linear", "bias", "activation"])

    output_layer = Structure()
    output_layer.add(Linear(
        n_categories,
        optimizer=optimizer,
    ), "linear")
    output_layer.add(Bias(optimizer=optimizer), "bias")
    output_layer.add(Logistic(), "activation")
    output_layer.connect_sequence(["linear", "bias", "activation"])

    model = Structure()

    # Normalize the incoming image data
    model.add(TrainingData(), "training_data")
    model.add(OnlineNormalization2D(), "input_norm")
    model.add(Bias(optimizer=optimizer), "input_shift")

    # Create two tiers of convolution blocks
    model.add(deepcopy(small_conv), "conv_0")
    model.add(deepcopy(small_conv), "conv_1")
    model.add(MaxPool2D(window=2, stride=2), "max_pool_1")
    model.add(deepcopy(large_conv), "conv_2")
    model.add(deepcopy(large_conv), "conv_3")

    # Flatten and pass to a couple of dense layers
    model.add(Flatten(), "flatten")
    model.add(dense_layer, "dense")
    model.add(output_layer, "output")

    # The label and evaluation path blocks
    model.add(Copy(), "predictions")
    model.add(OneHot(n_categories), "onehot")
    model.add(SquareLoss(), "loss")
    model.add(HardMax(), "hardmax")

    model.connect_sequence([
        "training_data",
        "input_norm",
        "input_shift",
        "conv_0",
        "conv_1",
        "max_pool_1",
        "conv_2",
        "conv_3",
        "flatten",
        "dense",
        "output",
        "predictions",
        "loss",
    ])

    model.connect("training_data", "onehot", i_port_tail=1)
    model.connect("onehot", "loss", i_port_head=1)
    model.connect("predictions", "hardmax", i_port_tail=1)

    return model


def train_model(model):
    os.makedirs(reports_dir, exist_ok=True)

    training_loss_logger = ValueLogger(
        log_scale=True,
        n_iter_report=n_iter_report,
        reports_dir=reports_dir,
        value_name="training_loss")

    for i_iter in range(n_iter_train):
        model.forward_pass()
        model.backward_pass()
        training_loss_logger.log_value(model.blocks["loss"].loss)
        if i_iter % n_iter_save == 0:
            model_to_save = deepcopy(model)
            model_to_save.remove("training_data")
            save_structure(model_filename, model_to_save)


if __name__ == "__main__":
    main()
