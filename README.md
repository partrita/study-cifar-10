# CIFAR-10 image classification 

A case study illustrating the use of a Cottonwood convolutional
neural network to categorize images from the CIFAR-10 data set.

This is a model that can be trained and tested on a laptop.
It's not going to break any records for performance, but
it will provide a rich platform for teaching the
concepts of convolutional neural networks and for
experimentating with novel algorithmic components.

If you would like a deeper dive into the operation of individual
pieces of this convolutional neural network, as well as the concepts
and code behind them, please join me in
[Course 322](https://e2eml.school/322) of the End to End Machine Learning
curriculum.

# Download this study

Get all of these files by navigating at the command line to the directory
where you want them to to live and cloning the repository:

    git clone https://gitlab.com/brohrer/study-cifar-10.git
    cd study-cifar-10

# Get the CIFAR-10 data set

CIFAR-10 is a collection of labeled images from ten different classes,
including cats and dogs, airplanes and frogs. There is a
[data description here](https://www.cs.toronto.edu/~kriz/cifar.html)
and history of state-of-the-art results in 
[the Wikipedia article](https://en.wikipedia.org/wiki/CIFAR-10).

To download CIFAR-10, use the link from 
[the description page](https://www.cs.toronto.edu/~kriz/cifar.html)
for the Python version to get a 186 MB gzipped tarball. Extract it to
get a directory of images called `cifar-10-batches-py` and make sure that
directory is moved here, into the same directory as `train.py` and `test.py`.

The dataset is wrapped in a Cottonwood [block](
https://gitlab.com/brohrer/study-cifar-10/-/blob/main/data.py)
that loads the images and labels
from the files, properly formats them, and feeds them, one per iteration,
to the classifier as it runs through its training and testing loops.

# Install Cottonwood

This classifier was created in [the Cottonwood machine learning framework](
https://e2eml.school/cottonwood). The recommended way to install
Cottonwood is from the git repository. At the command line:

    git clone https://gitlab.com/brohrer/cottonwood.git
    python3 -m pip install -e cottonwood
    cd cottonwood
    git checkout v29

It's important to check out the "v29" branch. Cottonwood has no guarantees
of backward compatibility and it evolves quickly.
If this case, `python3` is the command that invokes the Python 3.x interpreter.
On your machine it might just be `python`.

If you are new to Cottonwood, there's a pair of video walkthroughs of
the project [here](https://end-to-end-machine-learning.teachable.com/courses/322-convolutional-neural-networks-in-two-dimensions/lectures/24071959)
and [here](https://end-to-end-machine-learning.teachable.com/courses/322-convolutional-neural-networks-in-two-dimensions/lectures/24080930).

# Test the model

This study comes with a pre-trained model. To run it against the full set
of test images:

    python3 test.py

This should produce a performance summary and generate a confusion matrix
in a `reports` subdirectory.

# Get a set of detailed reports

To see structure diagrams, a detailed summary of the model structure
and all its hyperparameters, and visualizations of convolutional kernels:

    python3 report.py

This also generates a set of positive and negative examples to give you a
better sense of where the model is strong and where it struggles.

# Train the model

If you would like to train the model for yourself from scratch, or better yet,
make some changes and try to improve on it, I strongly encourage you to
modify your `train.py` and run:

    python3 train.py

This will create a running plot of the training loss in the reports
directory so you can track its progress.

# Reports

Here are some samples of the report outputs that get generated. 

### A sampling of positive examples

![correct examples](images/correct_examples.png)

### A sampling of negative examples

![incorrect examples](images/incorrect_examples.png)

### The confusion matrix of the testing results

![confusion matrix](images/confusion_matrix.png)

### A visualization of convolutional layers

![convolutional layer](images/convolutional_layer.png)

### A visualization of convolutional kernels

![convolutional kernel](images/convolutional_kernel.png)

### A diagram of the model structure

![structure diagram](images/structure_diagram.png)

### A training curve

![training curve](images/report_training_loss.png)
